from cryptography import x509
from cryptography.hazmat.backends import default_backend
import os
import shutil
from datetime import datetime
import logging

##> Import utilities!
from src.fullFetchVCert import fullFetchVCert

##> Do all steps neccessary to completely renew all certificates on a PLC, one by one!
def batchRenew(config, hook):

	##> Setup local folders!
	clear(config["localCertFolder"])

	##> Fetch all certificates from remote!
	hook.toCertFolder()
	hook.getAllCerts(config["localCertFolder"], config["certSuffix"])
	
	##> Check all local certificates for renewal!
	p = os.path.join(os.getcwd(), config["localCertFolder"])
	renew = []
	for fn in os.listdir(p):
		with open(os.path.join(p, fn), "rb") as f:
			cert = x509.load_der_x509_certificate(f.read(), default_backend())
			delay = (cert.not_valid_after - datetime.now()).total_seconds()
			if delay <= 0:
				renew.append(fn.split(".")[0])

	##> Remove local certs!
	clear(config["localCertFolder"])
	
	##> Fetch new certs for the ones that are to be replaced, then upload new certificate!
	for cn in renew:
		fullPath = fullFetchVCert(config, cn)
		hook.upload(fullPath, cn + config["certSuffix"])
		
	##> Remove local certs!
	clear(config["localCertFolder"])
		
##> Remove local certs!
def clear(f):
	if os.path.exists(f):
		shutil.rmtree(f)
	os.makedirs(f)