##> Config!
class config:

	def __init__(self):
		self.c = {
			"FTPHost": "<IP>",
			"FTPUser": "<USR>",
			"FTPPassword": "<PWD>",
			"FTPRemoteCertFolder": "/PATH/TO/CERTS",
			"localCertFolder": "LOCAL/CERTSTORE",
			"certSuffix": ".SUFFIX",
			"VENUser": "<USR>",
			"VENPassword": "<PWD>",
			"VENUrl": "<URL>",
			"VENZone": "<ZONE>",
			"CertificateRequestOrigin": "<ORIGIN>", ##> The standard value for Origin should be the VendorName and ProductName (unless VendorName=ProductName or it is a vendor-less open source project in which case it should simply be the ProductName). Please do not include the product version or any other information that will change over time.
		}
		
	def get(self):
		return self.c