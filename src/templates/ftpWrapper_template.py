##> Template of Wrapper for ftp!
class ftpWrapper:

	def __init__(self, host=None, user=None, password=None): ##> Initialization!
		self.host = host
		self.user = user
		self.password = password
		##> Implement Rest!
		
	def connect(self): ##> Establishes connection!
		pass ##> Implement!
		
	def disconnect(self): ##> Disconnects!
		pass ##> Implement!
			
	def download(self, hostFilename, localFilename): ##> Downloads a file from the server!
		pass ##> Implement!

	def upload(self, localFilename, hostFilename): ##> Uploads a file to the server!
		pass ##> Implement!
		
	def getCWD(self): ##> Returns current working directory!
		pass ##> Implement!
		
	def changeCWD(self, path): ##> Changes current working directory!
		pass ##> Implement!
		
	def list(self): ##> List directory contents!
		pass ##> Implement!
		
	def mkDir(self, path): ##> Creates a new directory on the server!
		pass ##> Implement!
		
	def rmDir(self, path): ##> Removes a directory on the server!
		pass ##> Implement!

	def rmFile(self, name): ##> Deletes a file on the server!
		pass ##> Implement!

###############################################################################################
##########  vv Class Setters vv  ##############################################################

	def setConnectionParameters(self, host=None, user=None, password=None):
		self.setHost(host)
		self.setUser(user)
		self.setPassword(password)
		
	def scp(self, host=None, user=None, password=None): ##> setConnectionParameters()!
		return self.setConnectionParameters(host, user, password)
		
	def setHost(self, host=None):
		self.host = host or self.host
	
	def setUser(self, user=None):
		self.user = user or self.user
	
	def setPassword(self, password=None):
		self.password = password or self.password
	
###############################################################################################
##########  vv Class Getters vv  ##############################################################

	def getConnectionParameters(self):
		return {
				"host": self.getHost(),
				"user": self.getUser(),
				"password": self.getPassword()
			}
			
	def gcp(self): ##> getConnectionParameters()!
		return self.getConnectionParameters()
	
	def getHost(self):
		return self.host
		
	def getUser(self):
		return self.user
		
	def getPassword(self):
		return self.password