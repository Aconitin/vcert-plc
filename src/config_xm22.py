##> Config pre-initialized with default values for Bosch Rexroth XM22!
class config:

	def __init__(self):
		self.c = {
			"FTPHost": "192.168.0.137", ##> Default. Can be found in reference!
			"FTPUser": "boschrexroth", ##> Default. Can be found in reference!
			"FTPPassword": "boschrexroth", ##> Default. Can be found in reference!
			"FTPRemoteCertFolder": "/OEM/ProjectData/CertificateStore/certs",
			"localCertFolder": "../certs",
			"certSuffix": ".der",
			"VENUser": "<USR>", ##> Needs to be added!
			"VENPassword": "<PWD>",
			"VENUrl": "<URL>",
			"VENZone": "<ZONE>",
			"CertificateRequestOrigin": "<ORIGIN>", ##> The standard value for Origin should be the VendorName and ProductName (unless VendorName=ProductName or it is a vendor-less open source project in which case it should simply be the ProductName). Please do not include the product version or any other information that will change over time.
		}
		
	def get(self):
		return self.c