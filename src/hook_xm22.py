import os
import logging

##> Hook for Bosch Rexroth xm22!
class hook:

	def __init__(self, wrapper, certFolder="/OEM/ProjectData/CertificateStore/certs"):
		self.wrapper = wrapper
		self.certFolder = certFolder ##> Default path as above!
		
	##> Change cwd to certificate folder!
	def toCertFolder(self):
		for f in self.certFolder.split("/"):
			if f != "":
				self.wrapper.changeCWD(f)
		
	##> List all certificates!
	def getCertList(self, suffix=".der"):
		return [e for e in self.wrapper.list() if e.endswith(suffix)]
		
	##> Download all certificates!
	def getAllCerts(self, folder, suffix=".der"):
		logging.info("Fetching all certificates ...")
		if not os.path.exists(folder):
			os.makedirs(folder)
		lst = self.getCertList(suffix)
		for e in lst:
			self.wrapper.download(e, os.path.join(os.getcwd(), folder, e))
		return lst
		
	##> Upload new certificate!
	def upload(self, localFilename, hostFilename):
		return self.wrapper.upload(localFilename, hostFilename)