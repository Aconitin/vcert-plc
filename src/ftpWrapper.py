##> Dependencies!
from ftplib import FTP
import logging

##> Wrapper for ftp!
class ftpWrapper:

	def __init__(self, host=None, user=None, password=None):
		self.host = host
		self.user = user
		self.password = password
		self.ftp = None	##> Instance of ftplib.FTP!
		
	def connect(self): ##> Establishes connection!
		try:
			self.ftp = FTP(self.host, self.user, self.password)
			logging.info("Connected!")
			return True
		except:
			return False
		
	def disconnect(self): ##> Disconnects!
		try:
			return self.ftp.quit()
		except:
			return self.ftp.close()
			
	def download(self, hostFilename, localFilename): ##> Downloads a file from the server!
		return self.ftp.retrbinary("RETR " + hostFilename, open(localFilename, "wb").write)

	def upload(self, localFilename, hostFilename): ##> Uploads a file to the server!
		return self.ftp.storbinary("STOR " + hostFilename, open(localFilename, "rb"))
		
	def getCWD(self): ##> Returns current working directory!
		return self.ftp.pwd()
		
	def changeCWD(self, path): ##> Changes current working directory!
		return self.ftp.cwd(path)
		
	def list(self): ##> List directory contents!
		ret = []
		self.ftp.retrlines("NLST", lambda e:ret.append(e))
		return ret
		
	def mkDir(self, path): ##> Creates a new directory on the server!
		return self.ftp.mkd(path)
		
	def rmDir(self, path): ##> Removes a directory on the server!
		return self.ftp.rmd(path)

	def rmFile(self, name): ##> Deletes a file on the server!
		return self.ftp.delete(name)

###############################################################################################
##########  vv Class Setters vv  ##############################################################

	def setConnectionParameters(self, host=None, user=None, password=None):
		self.setHost(host)
		self.setUser(user)
		self.setPassword(password)
		
	def scp(self, host=None, user=None, password=None): ##> setConnectionParameters()!
		return self.setConnectionParameters(host, user, password)
		
	def setHost(self, host=None):
		self.host = host or self.host
	
	def setUser(self, user=None):
		self.user = user or self.user
	
	def setPassword(self, password=None):
		self.password = password or self.password
	
###############################################################################################
##########  vv Class Getters vv  ##############################################################

	def getConnectionParameters(self):
		return {
				"host": self.getHost(),
				"user": self.getUser(),
				"password": self.getPassword()
			}
			
	def gcp(self): ##> getConnectionParameters()!
		return self.getConnectionParameters()
	
	def getHost(self):
		return self.host
		
	def getUser(self):
		return self.user
		
	def getPassword(self):
		return self.password