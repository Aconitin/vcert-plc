from asn1crypto import x509,pem
import logging
	
##> Converts a .pem certificate file into a .der certificate file!
def pemToDer(pemFile): ##> .pem filename without .pem ending!
	with open(pemFile + ".pem", "rb") as f:
		der_bytes = f.read()
		if pem.detect(der_bytes):
			type_name, headers, der_bytes = pem.unarmor(der_bytes)

	cert = x509.Certificate.load(der_bytes)

	with open(pemFile + ".der", "wb") as f:
		der_bytes = cert.dump()
		pem_bytes = pem.armor("CERTIFICATE", der_bytes)
		f.write(pem_bytes)
	logging.info("Converted .pem certificate into " + str(pemFile) + ".der")