![](docFiles/acc_logo.png)

# **VENAFI** ![](docFiles/venafi_logo.png) **<-> PLC**

This Python library provides a bridge between the [VENAFI](https://www.venafi.com/) Platform and [Bosch Rexroth PLCs](https://www.boschrexroth.com/en/xc/products/product-groups/electric-drives-and-controls/plc) for renewing digital certificates through the [VENAFI VCert](https://support.venafi.com/hc/en-us/articles/217991528-Introducing-VCert-API-Abstraction-for-DevOpsSec) API. It is designed to run on any [Bosch IoT Gateway](https://www.boschrexroth.com/de/de/produkte/produktgruppen/elektrische-antriebe-und-steuerungen/news/software-iot-gateway/index) as well as other ARM SBCs.

# **Installation**

## Prerequisites

- [Python](https://www.python.org/) 3.7+
- All prerequisites for the [VENAFI VCert Python Library](https://github.com/Venafi/vcert-python#prerequisites-for-using-with-trust-protection-platform)

## Useage

See `example1.py` & `example2.py`, e.g.:

```python
##> Create config, preinitialized with xm22 config for this example. For custom configs, use a template!
c = config().get()

##> Initialize ftp wrapper. You can use sftp by using an sftp wrapper!
w = ftpWrapper(c["FTPHost"], c["FTPUser"], c["FTPPassword"])

##> Try to establish an FTP connection to the PLC!
if w.connect():

	##> Create hook for all tasks relating to the PLC, in this case the Bosch Rexroth xm22!
	h = hook(w, c["FTPRemoteCertFolder"])

	##> Renew all certificates on the PLC by fetching new ones from the VENAFI platform!
	batchRenew(c, h)
	
	##> Disconnect ftp!
	w.disconnect()

```

## Templates

In order for this library to work with your setup, you have to configure it properly within three files:

- `config.py` holds configuration for your local certificate folder, your PLC configuration as well as your venafi platform data.

- `ftpWrapper.py` is used to establish an FTP connection to your PLC. You can just use the provided one, or switch to sFTP with e.g. `pysftp` by changing the wrapper accordingly.

- `hook.py` uses the `ftpWrapper` to download certificates from the PLC. You can use the provided one or add features as needed by your `ftpWrapper`.

Templates for all of these files can be found in the `/templates` folder.

# **Further information**

The design document can be found [here](https://gitlab.com/accessec/vcert-plc/tree/master/docFiles/ddv12.pdf).

## Context

Industry 4.0 and IoT are currently among the most important topics for the further development and optimization of industrial value creation networks. The terms stand for the vision of an intelligent factory or device that can quickly adapt dynamically to new tasks and that goes hand in hand with the digitization of the economy.

One requirement of the intelligent factory is that machines or devices are inter-connected, which results in corresponding threats to the industrial production facilities. Hence, these facilities must be adequately protected, meaning that IT security becomes a critical factor to these sys-tems. The trend in Industry 4.0 and IoT encourages the networking of machinery and devices, which make the attack surfaces grow even more. Both corporations with manufacturing facilities and SMEs in the manufacturing industry are facing increasing threats to their cyber-physical systems. Safeguarding the identity of devices and machines becomes even more relevant once smart devices and smart ma-chines are becoming “self-organizing” and need trustworthy and proven identities that they are able to authenticate with.

In the world of PLCs (Programmable Logic Controllers), there was no common proto-col stack for machine-to-machine communication, so the most de facto standard for PLC machine-to-machine communication became OPC UA. OPC UA describes the use of certificates to secure the com-munications, but lacks proper enrollment, management and revocation capabilities for these certificates, and so do most PLCs deployed, in design, or that will be developed in the foreseeable future. The lack of a common interface for machine identities means that errors, complexities, and new risks are created for deploying organizations.

## Example hardware setup

![](docFiles/png1.png)

## Example software flow

![](docFiles/png2.png)