'''
	Lists all certificates on a PLC!
'''

##> Import classes!
from src.config_xm22 import config
from src.hook_xm22 import hook
from src.ftpWrapper import ftpWrapper

##> Import utilities!
from src.batchRenew import batchRenew

##> Create config, preinitialized with xm22 config for this example. For custom configs, use a template!
c = config().get()

##> Initialize ftp wrapper. You can use sftp by using an sftp wrapper!
w = ftpWrapper(c["FTPHost"], c["FTPUser"], c["FTPPassword"])

##> Try to establish an FTP connection to the PLC!
if w.connect():

	##> Create hook for all tasks relating to the PLC, in this case the Bosch Rexroth xm22!
	h = hook(w, c["FTPRemoteCertFolder"])
	print(h.getCertList())
	
	##> Disconnect ftp!
	w.disconnect()
